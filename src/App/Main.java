package App;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("UI.fxml"));
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = loader.load();
        Controller controller=loader.getController();
        controller.Init();
        primaryStage.setTitle("2048");
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("icon.jpg")));
        Scene scene=new Scene(root);
        scene.setOnKeyPressed(controller);
        scene.setOnKeyReleased(controller);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
        controller.ImplInit(4);

    }


    public static void main(String[] args) {
        launch(args);
    }
}
